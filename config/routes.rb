Rails.application.routes.draw do
  get 'errors/not_found'
  get 'errors/internal_server_error'
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  resources :orders, only: [:index, :show, :payment, :order, :success, :destroy, :confirmation, :update]
  resources :types, only: [:index, :show]
  resources :collections, only: [:index, :show]
  resources :categories, only: [:index, :show]
  resources :products, only: [:index, :show]
  resources :pages, only: [:show]
  resources :order_items
  resource :cart, only: [:show]
  root "static#homepage"
  #get "/cart" => 'static#cart', as: "cart"
  get "/order" => "orders#order"
  get "/confirmation" => "orders#confirmation"
  get "/success" => "orders#success"
  put :delete_picture, controller: :products
  post :create_order, controller: :orders
  match "/404", :to => "errors#not_found", :via => :all
  match "/500", :to => "errors#internal_server_error", :via => :all
end
