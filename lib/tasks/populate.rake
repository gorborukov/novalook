namespace :populate do
  desc "Populate categories and types"
  task :categories => :environment do
    Type.destroy_all
    Category.destroy_all
    types = ['Одежда', 'Обувь', 'Сумки', 'Нижнее белье', 'Аксессуары', 'Распродажа']

    types.each do |type|
      Type.create!(name: type)
    end

    categories = [{"name" => 'Рубашки и кофты', "type_id" => Type.where("name" => 'Одежда').first.id},
                  {"name" => 'Куртки', "type_id" => Type.where("name" => 'Одежда').first.id},
                  {"name" => 'Вязаные вещи', "type_id" => Type.where("name" => 'Одежда').first.id},
                  {"name" => 'Юбки', "type_id" => Type.where("name" => 'Одежда').first.id},
                  {"name" => 'Верхняя одежда', "type_id" => Type.where("name" => 'Одежда').first.id},
                  {"name" => 'Футболки', "type_id" => Type.where("name" => 'Одежда').first.id},
                  {"name" => 'Топы', "type_id" => Type.where("name" => 'Одежда').first.id},
                  {"name" => 'Платья', "type_id" => Type.where("name" => 'Одежда').first.id},
                  {"name" => 'Брюки', "type_id" => Type.where("name" => 'Одежда').first.id},
                  {"name" => 'Одежда для отдыха', "type_id" => Type.where("name" => 'Одежда').first.id},
                  {"name" => 'Туфли', "type_id" => Type.where("name" => 'Обувь').first.id},
                  {"name" => 'Кроссовки', "type_id" => Type.where("name" => 'Обувь').first.id},
                  {"name" => 'Ботинки', "type_id" => Type.where("name" => 'Обувь').first.id},
                  {"name" => 'Сапоги', "type_id" => Type.where("name" => 'Обувь').first.id},
                  {"name" => 'Сандали', "type_id" => Type.where("name" => 'Обувь').first.id},
                  {"name" => 'Сумки', "type_id" => Type.where("name" => 'Сумки').first.id},
                  {"name" => 'Кошельки', "type_id" => Type.where("name" => 'Сумки').first.id},
                  {"name" => 'Клатчи', "type_id" => Type.where("name" => 'Сумки').first.id},
                  {"name" => 'Рюкзаки', "type_id" => Type.where("name" => 'Сумки').first.id},
                  {"name" => 'Сумочки для косметики', "type_id" => Type.where("name" => 'Сумки').first.id},
                  {"name" => 'Боди', "type_id" => Type.where("name" => 'Нижнее белье').first.id},
                  {"name" => 'Бюстгалтеры', "type_id" => Type.where("name" => 'Нижнее белье').first.id},
                  {"name" => 'Трусы', "type_id" => Type.where("name" => 'Нижнее белье').first.id},
                  {"name" => 'Украшения', "type_id" => Type.where("name" => 'Аксессуары').first.id},
                  {"name" => 'Шляпы', "type_id" => Type.where("name" => 'Аксессуары').first.id},
                  {"name" => 'Перчатки', "type_id" => Type.where("name" => 'Аксессуары').first.id},
                  {"name" => 'Шарфы', "type_id" => Type.where("name" => 'Аксессуары').first.id},
                  {"name" => 'Носки', "type_id" => Type.where("name" => 'Аксессуары').first.id}
                  ]

    categories.each do |category|
      Category.create!(name: category['name'], type_id: category['type_id'])
    end
  end
end
