module Adapter
  class Store

    attr_accessor :product

    def initialize(product)
      @product = product
      @agent = Mechanize.new
      @agent.history_added = Proc.new { sleep(0.3)}
      @agent.user_agent_alias = 'Mac Safari'
      @color = []
      @size = []
      @price = ''
      @images = []
    end

    def get_fields
      if (@product.link && @product.fetch)
        endpoint = @product.link
        page = @agent.get(endpoint)
        block = page.css('.product-attribute-main')
        colors = (block.css('ul#j-sku-list-1 li') unless nil)
        sizes = (block.css('ul#j-sku-list-2 li') unless nil)
        images = (page.css('ul.image-thumb-list li') unless nil)
        if colors
          colors.each do |color|
            if color.css('img').first
              @color << {
                color: color.css('img').first.attr('title'),
                image: color.css('img').first.attr('src')
              }
            else
              @color << {
                color: color.css('span').first.attr('title')
              }
            end
          end
        end
        if sizes
          sizes.each do |size|
            @size << {
              value: (size.css('span').first.text rescue nil)
            }
          end
        end
        if images
          images.each do |image|
            @images << image.css('img').first.attr('src').gsub('_50x50.jpg','')
          end
        end
        @price = (page.css('#j-sku-discount-price').text.split("-")[1] || page.css('#j-sku-discount-price').text.split("-")[0] || page.css('#j-sku-price').text.split("-")[0])
        if @price.to_s.include? "\u00A0" #chinese whitespaces fix
          @price = @price.gsub("\u00A0", "").to_i
        else 
          @price = @price.to_i
        end
        product.color = @color
        product.size = @size
        product.price = @price
        product.additional = @images
      end
    end

  end
end
