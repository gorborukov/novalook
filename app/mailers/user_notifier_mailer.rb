class UserNotifierMailer < ApplicationMailer
  default :from => 'notifer@novalook.ru'

  def send_order_confirmation(email, uid, name)
  	@name = name
  	@uid = uid
    mail( :to => email,
    :subject => "Novalook.ru — заказ №#{uid}" )
  end
end
