require 'sendgrid-ruby'
include SendGrid
class OrdersController < ApplicationController
  before_action :set_order, only: [:show, :edit, :update, :destroy]
  def order
    if (session[:order_id] == current_order.id)
      @order = current_order
      @order.uid ||= SecureRandom.random_number(1000000000)
      @order.items = current_order.order_items.to_json
      @order.status = "Создан"
      @order.save
    else
      not_found
    end
  end 

  def confirmation
    @order = current_order
    UserNotifierMailer.send_order_confirmation(@order.email, @order.uid, @order.name).deliver
    @order.status = "Ожидает оплаты"
    @order.save
  end

=begin
  skip_before_action :verify_authenticity_token
  def create_order
    Order.create(
      uid: params[:uid],
      items: params[:items].to_json,
      email: params[:email],
      phone: params[:phone],
      name: params[:name],
      zipcode: params[:zipcode],
      country: params[:country],
      city: params[:city],
      address: params[:address],
      tin: params[:tin],
      passport: params[:passport],
      payment_method: params[:payment_method],
      status: 'СОЗДАН'
    )
    UserNotifierMailer.send_order_confirmation(params[:email], params[:uid], params[:name]).deliver
    #sg = SendGrid::API.new(api_key: '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')
  end
=end

  def payment
  end

  def success
    if current_order
      @order = current_order
      @order.status = "Оплачен"
      @order.save
      session.delete(:order_id)
    else
      not_found
    end
  end
  # GET /orders
  # GET /orders.json
  def index
    @orders = Order.all
  end

  # GET /orders/1
  # GET /orders/1.json
  def show
  end

  # GET /orders/new
  def new
    @order = Order.new
  end

  # GET /orders/1/edit
  def edit
  end

  # POST /orders
  # POST /orders.json
  def create
    @order = Order.new(order_params)

    respond_to do |format|
      if @order.save
        format.html { redirect_to confirmation_path, notice: 'Order was successfully updated.' }
      else
        format.html { redirect_to cart_path }
      end
    end
  end

  # PATCH/PUT /orders/1
  # PATCH/PUT /orders/1.json
  def update
    @order = current_order

    respond_to do |format|
      if @order.update(order_params)
        format.html { redirect_to confirmation_path, notice: 'Order was successfully updated.' }
      else
        format.html { redirect_to cart_path }
      end
    end
  end

  # DELETE /orders/1
  # DELETE /orders/1.json
  def destroy
    @order = current_order
    @order.destroy
    session.delete(:order_id)
    redirect_to cart_path
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_order
      @order = Order.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def order_params
      params.require(:order).permit(:uid, :items, :email, :phone, :name, :address, :passport, :status, :tracking_id, :payment_method, :zipcode, :country, :city, :tin, :total)
    end
end
