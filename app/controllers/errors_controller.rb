class ErrorsController < ApplicationController
  def not_found
  	@products = Product.where(featured: true).limit(8).order("RANDOM()")
  end

  def internal_server_error
  	@products = Product.where(featured: true).limit(8).order("RANDOM()")
  end
end
