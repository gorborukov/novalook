class ApplicationController < ActionController::Base

  helper_method :current_order

  def not_found
	raise ActionController::RoutingError.new('Not Found')
  end

  def current_order
    if session[:order_id]
      Order.find(session[:order_id])
    else
      Order.new
    end
  end
end
