class ProductsController < ApplicationController
  before_action :set_product, only: [:show, :edit, :update, :destroy]

  # GET /products
  # GET /products.json
  def index
    @products = Product.all
  end

  # GET /products/1
  # GET /products/1.json
  def show
    @photos = []
    @product.pictures.each do |picture|
      @photos << picture.attachment.url(:medium)
    end
    @order_item = current_order.order_items.new
    set_meta_tags title: @product.name + ' купить',
                  keywords: @product.name.split(' ').join(","),
                  og: { title: @product.name + ' купить', type: 'product', url: request.original_url }
  end

  # GET /products/new
  def new
    @product = Product.new
  end

  # POST /products
  # POST /products.json
  def create
    @product = Product.new(product_params)

    respond_to do |format|
      if @product.save
        if params[:attachments]
          params[:attachments].each { |attachment|
            @product.pictures.create(attachment: attachment)
          }
        end
        format.html { redirect_to @product, notice: 'Product was successfully created.' }
        format.json { render :show, status: :created, location: @product }
      else
        format.html { render :new }
        format.json { render json: @product.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /products/1
  # PATCH/PUT /products/1.json
  def update
    respond_to do |format|
      if @product.update(product_params)
        if params[:attachments]
          params[:attachments].each { |attachment|
            @product.pictures.create(attachment: attachment)
          }
        end
        format.html { redirect_to @product, notice: 'Product was successfully updated.' }
        format.json { render :show, status: :ok, location: @product }
      else
        format.html { render :edit }
        format.json { render json: @product.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /products/1
  # DELETE /products/1.json
  def destroy
    @product.destroy
    respond_to do |format|
      format.html { redirect_to products_url, notice: 'Product was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def delete_picture
    picture = Picture.find(params[:id])
    picture.destroy
    redirect_back(fallback_location: root_path)
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_product
      @product = Product.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def product_params
      params.require(:product).permit(:name, :sku, :link, :price, :rating, :color, :size, :description, :stock, :category_id, :collection_id, :type_id, :reviews, :additional, :availability, :fetch, :featured, pictures_attributes: [:id, :attachment, :product_id, :_destroy])
    end
end
