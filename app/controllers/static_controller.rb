class StaticController < ApplicationController
	def homepage
		@products = Product.all
		@collection = Collection.where(featured: true).first
		@collection_products = (Product.where(collection_id: @collection.id) unless nil)
	end
end
