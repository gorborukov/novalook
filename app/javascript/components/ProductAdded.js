import React from "react"
import PropTypes from "prop-types"

class ProductAdded extends React.Component {
  constructor(props) {
      super(props);
  }

  reloadWindow = (e) => {
  	window.location.reload();
  }
  
  render () {

    return (
      <React.Fragment>
      	<div className="modal fade" ref={modal => this.modal = modal} id="productAddedModal" tabIndex="-1" role="dialog" aria-labelledby="productAddedModalLabel" aria-hidden="true">
		  <div className="modal-dialog" role="document">
		    <div className="modal-content">
		      <div className="modal-header">
		        <h5 className="modal-title" id="exampleModalLabel">Поздравляем!</h5>
		        <button type="button" className="close" data-dismiss="modal" aria-label="Close" onClick={this.reloadWindow}>
		          <span aria-hidden="true">&times;</span>
		        </button>
		      </div>
		      <div className="modal-body">
		        Товар добавлен в корзину.
		      </div>
		      <div className="modal-footer">
		        <button type="button" className="btn btn-secondary" data-dismiss="modal" onClick={this.reloadWindow}>Продолжить покупки</button>
		        <a href="/cart" className="btn btn-danger">Оформить заказ</a>
		      </div>
		    </div>
		  </div>
		</div>
      </React.Fragment>
    );
  }
}

export default ProductAdded