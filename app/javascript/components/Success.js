import React from "react"
import PropTypes from "prop-types"


class Success extends React.Component {
  constructor(props) {
      super(props);
      this.state = {
      	order: JSON.parse(localStorage.getItem("order"))
	  }
  }

  componentDidMount() {
  	localStorage.clear();
  }

  render () {
    const success = []
  	if (this.state.order !== null) {
      success.push(
        <div>
          <h1>Заказ успешно оформлен!</h1>
          <p>Номер вашего заказа <strong>{this.state.order.uid}</strong>, заказ будет сформирован и отправлен в течение двух рабочих дней. Для отслеживания заказа воспользуйтесь нашим сервисом.</p>
        </div>
      ); 
    } else {
      success.push(
        <div>
          <h1>404 Страница не найдена</h1>
          <p>Запрашиваемая страница почему-то не найдена</p>
        </div>
      );
    }
    return (
      <React.Fragment>
        <div className="row">
          <div className="col-12">
            <div className="success text-center">
            {success}
            </div>
      	  </div>
      	</div>
      </React.Fragment>
    );
  }
}

export default Success