import React from "react"
import PropTypes from "prop-types"


class Payment extends React.Component {
  constructor(props) {
      super(props);
      this.state = {
      	order: JSON.parse(localStorage.getItem("order"))
	  }
  }

  cancelOrder = () => {
  	localStorage.clear();
  	window.location.href = '/';
  }

  render () {
  	if (this.state)
    return (
      <React.Fragment>
        <div className="row">
          <div className="col-12">
            <div className="payment">
	            <table>
	              <td>
	                <tr>Номер вашего заказа:</tr>
	                <tr><span className="order-number">{this.state.order.uid}</span></tr>
	                <tr>К оплате:</tr>
	                <tr><strong>{this.state.order.total} &#8381;</strong></tr>
	                <tr>
	                	<form method="POST" action="https://money.yandex.ru/quickpay/confirm.xml">
    			      		  <input type="hidden" name="receiver" value="410012035561254" />
    			      		  <input type="hidden" name="quickpay-form" value="shop" />
    			      		  <input type="hidden" name="targets" value={"Перевод для заказа №" + this.state.order.uid + " novalook.ru"} />
    			      		  <input type="hidden" name="paymentType" value="AC"/>
    			      		  <input type="hidden" name="sum" value={this.state.order.total} />
                      <input type="hidden" name="successURL" value="https://novalook.ru/success"/>
    			      		  <input type="submit"className="btn btn-lg btn-danger"/>
			      		     </form>
	                </tr>
	              </td>
	      		</table>
      		</div>
      	  </div>
      	</div>
      </React.Fragment>
    );
  }
}

export default Payment