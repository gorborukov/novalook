import React from "react"
import PropTypes from "prop-types"


class CartItem extends React.Component {
  constructor(props) {
      super(props);
      this.state = {
		    item: props.data,
        index: props.index
	    }
  }

  /*getItemTotal () {
  	var total = parseInt(this.state.item.quantity)*parseInt(this.state.item.price)
  	return total
  }*/

  removeItem = () => {
    var storage = JSON.parse(localStorage.getItem("items"))
    storage.splice(this.state.index, 1)
    localStorage.setItem("items", JSON.stringify(storage))
    window.location.reload();
  }

  render () {
    return (
      <React.Fragment>
      	<tr className="cart-items">
      	  <td>
            <a href={'/products/' + this.state.item.id}>{this.state.item.name}</a><br/>
            <div className="cart-item-properties">{this.state.item.size} {this.state.item.color}</div>
          </td>
      	  <td>{this.state.item.quantity} x {this.state.item.price} &#8381;</td>
          <td><i className="fas fa-trash-alt" onClick={this.removeItem}></i></td>
      	</tr>
      </React.Fragment>
    );
  }
}

export default CartItem