import React from "react"
import PropTypes from "prop-types"
import CartItem from "./CartItem"
import ProductCounter from "./ProductCounter"


class Cart extends React.Component {
  constructor(props) {
      super(props);
      this.state = {
		items: [],
		process: 'visible'
	  }
  }
  componentDidMount() {
  	this.setState({items: JSON.parse(localStorage.items)})
  }

  clearCart = (e) => {
  	this.setState({items: [], process: 'hidden'}, () => {
  		localStorage.clear()
  		ProductCounter.setState({items: '0'})	
  	})
    window.location.reload();
    $(this.modal).modal('show');
  }

  getTotalPrice () {
    var total = []
    JSON.parse(localStorage.getItem("items")).map((item) => {
      total.push(parseInt(item.price)*parseInt(item.quantity))
    })
    var sum = total.reduce((a,b) => {
      return a + b;
    })
    return sum
  }

  newOrder = () => {
    window.location.href = '/order';
  }
  
  render () {
  	var items = this.state.items.map((item, index) => {
      return (
      	<CartItem data={item} index={index} key={index}/>
      )
    });

    return (
      <React.Fragment>
        <table className="table">
            <thead>
		        <tr>
		      		<th scope="col">Название</th>
		      		<th scope="col">Цена</th>
              <th scope="col">&nbsp;</th>
		    	</tr>
	    	</thead>
	    	  <tbody>
      			{items}
      		</tbody>
      	</table>
        <div className="cart-confirm">
          <p>Всего к оплате: <strong>{this.getTotalPrice()} &#8381;</strong></p>
        	<div className={this.state.process}>
        		<button className="btn btn-lg btn-danger" onClick={this.newOrder}>Перейти к оформлению заказа</button>
            <span onClick={this.clearCart} className="clear-cart">Очистить корзину</span>
        	</div>
        </div>
      </React.Fragment>
    );
  }
}

export default Cart