import React from "react"
import PropTypes from "prop-types"

class ProductCounter extends React.Component {
  constructor(props) {
      super(props);
      this.state = {
		items: localStorage.items
	  }
  }

  
  render () {
  	var items = this.state.items
  	if (items) {
  		items = JSON.parse(this.state.items).length
  	} else {
  		items = '0'
  	}

    return (
      <React.Fragment>
      	{items}
      </React.Fragment>
    );
  }
}

export default ProductCounter