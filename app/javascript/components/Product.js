import React from "react"
import PropTypes from "prop-types"
import ProductAdded from "./ProductAdded"

class Product extends React.Component {
  constructor(props) {
      super(props);
      this.handleChange = this.handleChange.bind(this);
	  this.state = {
		product: props.product,
		size: '',
		color: '',
		quantity: 1
	  }
  }

  addToCart = (e) => {
    e.preventDefault();

    var productJSON = {
    	'id': this.state.product.id,
    	'name': this.state.product.name,
    	'quantity': this.state.quantity,
    	'size': this.state.size,
    	'color': this.state.color,
    	'price': this.state.product.price
    }
    let items = localStorage.items ? JSON.parse(localStorage.items) : []
    items.push(productJSON);
    localStorage.setItem('items', JSON.stringify(items));
  }

  handleChange(event) {
    this.setState({ [event.target.name]: event.target.value });
  }
  
  render () {

  	var sizes = this.state.product.size.map(function(size, index){
  	  return (
  	  	<option key={index} name={index} value={size['value']}>{size['value']}</option>
  	  )
    });

    var colors = this.state.product.color.map(function(color, index){
  	  return (
  	  	<option key={index} name={index} value={color['color']}>{color['color']}</option>
  	  )
    });
    return (
      <React.Fragment>
	    <form className="order-form">
	      <p>
            Размер:
            <select name="size" className="form-control" onChange={this.handleChange} required>
            <option selected disabled>Выберите размер</option>
            {sizes}
            </select>
            <a href="" className='small size-table'>Таблица размеров</a>
          </p>
          <p>
            Цвет:
            <select name="color" className="form-control" onChange={this.handleChange} required>
            {colors}
            <option selected disabled>Выберите цвет</option>
            </select>
          </p>
          <p>
            Количество:
            <select name="quantity" className="form-control" onChange={this.handleChange}>
              <option>1</option>
              <option>2</option>
              <option>3</option>
            </select>
          </p>
          <p className="price">
            <strong>{this.state.product.price} &#8381; </strong><span className="free-delivery">+ бесплатная доставка</span>
          </p>
          <div className="cta">
            <span className="add">
              <a className="btn btn-lg btn-outline-danger" data-toggle="modal" data-target="#productAddedModal" onClick={this.addToCart}>Добавить в корзину</a>
            </span>
          </div>
	    </form>
	    <ProductAdded/>
      </React.Fragment>
    );
  }
}

export default Product