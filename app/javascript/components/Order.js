import React from "react"
import axios from "axios"
import PropTypes from "prop-types"
import shortid from "shortid"


class Order extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
    	uid: shortid.generate(),
		items: JSON.parse(localStorage.getItem("items")),
		total: this.getTotalPrice(),
		name: '',
		email: '',
		phone: '',
		zipcode: '',
		country: 'Россия',
		city: '',
		address: '',
		passport: '',
		tin: '',
		payment_method: '',
		alert: '' 
	}
  }

  componentDidMount () {
  	console.log(this.state.uid)
  }

  onChange = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  }

  onPaymentMethodChange = (e) => {
    this.setState({ payment_method: e.target.value });
  }

  getTotalPrice () {
    var total = []
    JSON.parse(localStorage.getItem("items")).map((item) => {
      total.push(parseInt(item.price)*parseInt(item.quantity))
    })
    var sum = total.reduce((a,b) => {
      return a + b;
    })
    return sum
  }

  onSubmit = (e) => {
  	e.preventDefault(e);
  	this.setState({
      alert: 'Идет оформление заказа…'
    });
    /*var items = [];
    JSON.parse(localStorage.getItem("items")).map((item) => {
      items.push(item)
    })*/
  	const { uid, items, total, name, email, phone, zipcode, country, city, address, passport, tin, payment_method } = this.state;
  	axios.post('/create_order',
      { uid,
      	items,
      	total,
       	name,
       	email,
       	phone,
       	zipcode,
       	country,
       	city,
       	address,
       	passport,
       	tin,
       	payment_method })
        .then((response) => {
        this.setState({
            alert: 'Заказ оформлен!'
      	});
    })
    .catch((error) => {
      this.setState({
        alert: 'Ошибка!'
      });
    });
    localStorage.setItem('order', JSON.stringify({'uid': uid, 'total': total, 'payment_method': payment_method }));
    window.location.href = '/payment';
  }

  render () {
  	const { total, name, email, phone, zipcode, country, city, address, passport, tin, payment_method } = this.state;
  	const alertBlock = [];
    if (this.state.alert != '') {
      alertBlock.push(<div className="alert alert-info" role="alert">{this.state.alert}</div>);
    }
    return (
      <React.Fragment>
        <div className="row">
          <div className="col-lg-3">
            <div className="order-total">
                <p>Ваш заказ на сумму:</p>
            	<div className="order-amount">{this.getTotalPrice()} &#8381;</div>
            	<span className="tip">доставка бесплатно</span>
            </div>
          </div>
          <div className="col-lg-9 order-data">
	          <h2>Оформление заказа</h2>
	          <form className="order-form" onSubmit={this.onSubmit}>
	              <div className="form-group">
				    <label>Ваши фамилия, имя и отчество:</label>
				    <input type="text" className="form-control" placeholder="Иванова Ирина Ивановна" name="name" value={name} onChange={this.onChange} required/>
				  </div>
				  <div className="form-row">
				    <div className="form-group col-md-6">
				      <label>Адрес электронной почты:</label>
				      <input type="email" className="form-control" placeholder="mail@mail.ru" name="email" value={email} onChange={this.onChange} required/>
				    </div>
				    <div className="form-group col-md-6">
				      <label>Контактный телефон:</label>
				      <input type="phone" className="form-control" placeholder="+79991234567" name="phone" value={phone} onChange={this.onChange} required/>
				    </div>
				  </div>
				  <hr/>
				  <div className="form-row">
				    <div className="form-group col-md-2">
				      <label>Индекс:</label>
				      <input type="text" className="form-control" placeholder="523456" name="zipcode" value={zipcode} onChange={this.onChange} required/>
				    </div>
				    <div className="form-group col-md-4">
				      <label>Страна</label>
				      <select className="form-control" value={country} name="country" onChange={this.onChange}>
				        <option>Россия</option>
				        <option>Украина</option>
				        <option>Казахстан</option>
				        <option>Германия</option>
				        <option>Израиль</option>
				        <option>США</option>
				        <option>ОАЭ</option>
				        <option>Турция</option>
				      </select>
				    </div>
				    <div className="form-group col-md-6">
				      <label>Город:</label>
				      <input type="text" className="form-control" placeholder="Москва" name="city" value={city} onChange={this.onChange} required/>
				    </div>
				  </div>
				  <div className="form-group">
				    <label>Адрес:</label>
				    <textarea className="form-control" placeholder="ул. Ленина, д. 12, кв. 34" name="address" value={address} onChange={this.onChange} required/>
				  </div>
				  <hr/>
				  <div className="alert alert-primary small" role="alert">
					  В соответствии с приказом Федеральной Таможенной Службы России от 24.11.2017 № 1861 
					  при прохождении вашего заказа через границу Российской Федерации, транспортной компании, 
					  выполняющей доставку, могут потребоваться для предоставления в таможенный орган реквизиты 
					  документа, удостоверяющего личность получателя, и его ИНН.
				  </div>
				  <div className="form-group">
				      <label>Паспорт (серия, номер, кем выдан, дата выдачи):</label>
				      <textarea className="form-control" placeholder="0303 777555 ОУФМС России по Московской области 12.12.2008" name="passport" value={passport} onChange={this.onChange} required/>
				      <span className="small">Мы гарантируем сохранность ваших данных в зашифрованном виде и обязуемся не передавать их третьим лицам</span>
				  </div>
				  <div className="form-group">
				      <label>ИНН (12 символов):</label>
				      <input type="text" className="form-control" placeholder="123456789101" name="tin" value={tin} onChange={this.onChange} required/>
				      <span className="small">Не знаете ИНН? Узнайте его с помощью сервиса на сайте ИФНС по ссылке <strong>https://service.nalog.ru/inn.do</strong></span>
				  </div>
				  <p>Выберите способ оплаты:</p>
				  <div onChange={this.onPaymentMethodChange.bind(this)}>
					  <div className="form-check">
						  <input className="form-check-input" name="payment" type="radio" value="card"/>
						  <label className="form-check-label">
						    Карта VISA/MasterCard
						  </label>
					  </div>
					  <div className="form-check">
						  <input className="form-check-input" name="payment" type="radio" value="bank" disabled/>
						  <label className="form-check-label">
						    Альфа-Банк (перевод)
						  </label>
					  </div>
					  <div className="form-check">
						  <input className="form-check-input" name="payment" type="radio" value="yandex" disabled/>
						  <label className="form-check-label">
						    Яндекс.Деньги
						  </label>
					  </div>
					  <div className="form-check">
						  <input className="form-check-input" name="payment" type="radio" value="sberbank" disabled/>
						  <label className="form-check-label">
						    Сбербанк Онлайн
						  </label>
					  </div>
				  </div>
				  <button type="submit" className="btn btn-primary btn-lg">Перейти к оплате</button>
				  <br/>
				  {alertBlock}
			  </form>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default Order