ActiveAdmin.register Product do
  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  permit_params :name, :sku, :link, :price, :rating, :color, :size, :description, :stock, :category_id, :collection_id, :type_id, :reviews, :additional, :availability, :fetch, :featured, pictures_attributes: [:id, :attachment, :product_id, :_destroy]
  #
  # or
  #
  # permit_params do
  #   permitted = [:permitted, :attributes]
  #   permitted << :other if params[:action] == 'create' && current_user.admin?
  #   permitted
  # end
  show do |product|
    attributes_table_for resource do
      row :id
      row :name
      row :sku
      row :link
      row :price
      row :rating
      row :color
      row :size
      row :description
      row :stock
      row :category_id
      row :collection_id
      row :type_id
      row :reviews
      row :additional
      row :availability
      row :fetch
      row :featured
      row :picture do
	    ul do
	      product.pictures.each do |picture|
	        li do 
	          image_tag(picture.attachment.url(:thumb))
	        end
	      end
	    end
	  end
    end
  end
  form do |f|
    f.inputs 'Details' do
      f.input :name
      f.input :sku
      f.input :link
      f.input :rating
      f.input :price
      f.input :category
      f.input :collection
      f.input :description
      f.input :size, as: :string
      f.input :color, as: :string
      f.input :fetch
      f.input :featured
      f.input :stock
      f.has_many :pictures, allow_destroy: true do |picture|
      	picture.input :attachment, hint: picture.template.image_tag(picture.object.attachment.url(:thumb))
      end
      #file_field_tag "attachments[]", type: :file, multiple: true
      #@product.pictures.each do |picture|
      #  image_tag picture.attachment.url(:thumb)
      #  link_to '×', {action: :delete_picture, id: picture.id}, method: :put, class: 'delete'
      #end
      
    end
    f.actions
  end
end

