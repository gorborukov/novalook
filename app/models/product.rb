class Product < ApplicationRecord
  belongs_to :type, optional: true
  belongs_to :category, optional: true
  belongs_to :collection, optional: true
  has_many :pictures, :dependent => :destroy, :validate => false
  accepts_nested_attributes_for :pictures, allow_destroy: true

  before_save :update_properties
  after_save :update_attachments

  self.per_page = 12

  def pictures_from_urls(urls)
    urls.each do |url|
      self.pictures.create(attachment: open(url))
    end
  end

  def computed_price
    price = (self.price.split("-")[1] || self.price.split("-")[0]).to_i
    percentage_price = price + ((price/100)*33) #33%UP
    percentage_price.round(-2)-1 #99 ROUND
  end

  private
    def update_properties
      updater = Adapter::Store.new(self)
      updater.get_fields
    end

    def update_attachments
      if !self.pictures.first
        self.pictures_from_urls(self.additional)
      end
    end
end
