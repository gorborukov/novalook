class Category < ApplicationRecord
  belongs_to :type, optional: true
  has_many :products
end
