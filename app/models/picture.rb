class Picture < ApplicationRecord
  belongs_to :product, optional: true
  has_attached_file :attachment, styles: { medium: "400x400#", thumb: "40x60>", original: "600x900>" }, :convert_options => { :thumb => '-quality 100' }, default_url: "/images/:style/missing.png"
  validates_attachment_content_type :attachment, content_type: /\Aimage\/.*\z/
end