class AddFeaturedToCollection < ActiveRecord::Migration[5.2]
  def change
    add_column :collections, :featured, :boolean
  end
end
