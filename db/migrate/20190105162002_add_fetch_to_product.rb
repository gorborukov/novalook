class AddFetchToProduct < ActiveRecord::Migration[5.2]
  def change
    add_column :products, :fetch, :boolean
  end
end
