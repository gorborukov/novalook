class AddZipcodeAndCityAndTinToOrder < ActiveRecord::Migration[5.2]
  def change
    add_column :orders, :zipcode, :string
    add_column :orders, :city, :string
    add_column :orders, :tin, :string
  end
end
