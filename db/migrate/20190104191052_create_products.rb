class CreateProducts < ActiveRecord::Migration[5.2]
  def change
    enable_extension 'citext'
    create_table :products do |t|
      t.string :name
      t.string :sku
      t.string :link
      t.string :price
      t.integer :rating
      t.jsonb :color, null: false, default: '{}'
      t.jsonb :size, null: false, default: '{}'
      t.text :description
      t.boolean :stock
      t.integer :category_id
      t.integer :collection_id
      t.jsonb :reviews, null: false, default: '{}'
      t.jsonb :additional, null: false, default: '{}'

      t.timestamps
    end
    add_index  :products, :color, using: :gin
    add_index  :products, :size, using: :gin
    add_index  :products, :reviews, using: :gin
    add_index  :products, :additional, using: :gin
  end
end
