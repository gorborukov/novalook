class AddTypeIdToProduct < ActiveRecord::Migration[5.2]
  def change
    add_column :products, :type_id, :integer
  end
end
