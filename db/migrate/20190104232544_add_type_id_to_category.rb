class AddTypeIdToCategory < ActiveRecord::Migration[5.2]
  def change
    add_column :categories, :type_id, :integer
  end
end
