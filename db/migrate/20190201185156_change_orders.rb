class ChangeOrders < ActiveRecord::Migration[5.2]
  def change
  	change_column :orders, :items, :jsonb, null: true, default: {}
  end
end
