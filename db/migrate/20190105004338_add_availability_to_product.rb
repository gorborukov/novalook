class AddAvailabilityToProduct < ActiveRecord::Migration[5.2]
  def change
    add_column :products, :availability, :string
  end
end
