class CreateOrders < ActiveRecord::Migration[5.2]
  def change
    create_table :orders do |t|
      t.string :uid
      t.jsonb :items, null: false, default: '{}'
      t.string :email
      t.string :phone
      t.string :name
      t.text :address
      t.text :passport
      t.string :status
      t.string :tracking_id
      t.string :payment_method

      t.timestamps
    end
    add_index :orders, :items, using: :gin
  end
end
